extends Sprite

var SPEED = 300

func _ready():
	pass # Replace with function body.

func _process(delta):
	var mX = Input.get_action_strength("right") - Input.get_action_strength("left")
	var mY = Input.get_action_strength("down") - Input.get_action_strength("up")
	var movement = Vector2(mX, mY)
	position += movement.normalized() * SPEED * delta
	
	look_at(get_global_mouse_position())
